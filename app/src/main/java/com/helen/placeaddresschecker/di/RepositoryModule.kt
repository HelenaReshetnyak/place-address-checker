package com.helen.placeaddresschecker.di

import com.helen.placeaddresschecker.di.base.DIModule
import com.helen.placeaddresschecker.repository.GeocodingRepository
import com.helen.placeaddresschecker.storage.GeocodingStorage
import org.koin.dsl.module

object RepositoryModule : DIModule {
    override fun getDependencies() = module {
        single<GeocodingRepository> { GeocodingStorage() }
    }
}