package com.helen.placeaddresschecker.di.base

import org.koin.core.module.Module

interface DIModule {
    fun getDependencies() : Module
}