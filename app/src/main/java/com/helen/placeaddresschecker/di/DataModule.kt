package com.helen.placeaddresschecker.di

import com.helen.placeaddresschecker.data.mapbox.MapboxGeocodingProvider
import com.helen.placeaddresschecker.data.provider.GeocodingProvider
import com.helen.placeaddresschecker.di.base.DIModule
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.mapboxsdk.geometry.LatLng
import org.koin.dsl.module

object DataModule : DIModule {
    override fun getDependencies() = module {
        single<GeocodingProvider<LatLng, CarmenFeature>> { MapboxGeocodingProvider() }
    }
}