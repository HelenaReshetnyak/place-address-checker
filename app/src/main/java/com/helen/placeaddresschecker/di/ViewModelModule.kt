package com.helen.placeaddresschecker.di

import com.helen.placeaddresschecker.di.base.DIModule
import com.helen.placeaddresschecker.ui.main.map.MapBoxViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ViewModelModule : DIModule {
    override fun getDependencies() = module {
        viewModel { MapBoxViewModel() }
    }
}