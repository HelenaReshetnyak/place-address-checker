package com.helen.placeaddresschecker.repository

import com.helen.placeaddresschecker.model.GeocodingResponse
import com.mapbox.mapboxsdk.geometry.LatLng

interface GeocodingRepository {
    suspend fun reverseGeocode(location: LatLng) : GeocodingResponse
}