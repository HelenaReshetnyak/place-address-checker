package com.helen.placeaddresschecker.exception

object AddressNotFoundException : Exception()