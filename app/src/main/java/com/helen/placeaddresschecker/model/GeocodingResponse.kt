package com.helen.placeaddresschecker.model

data class GeocodingResponse(
    val placeName: String,
    val address: String
)