package com.helen.placeaddresschecker.base

import android.annotation.SuppressLint
import android.util.Log.e
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel(), CoroutineScope {
    private val job = Job()

    @SuppressLint("LogNotTimber")
    private val exceptionHandler =
        CoroutineExceptionHandler { _, throwable ->
            e("ViewModel", throwable.message ?: "empty")
        }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job + exceptionHandler

    override fun onCleared() {
        job.cancel()
    }
}