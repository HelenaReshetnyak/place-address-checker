package com.helen.placeaddresschecker

import android.app.Application
import com.helen.placeaddresschecker.di.DataModule
import com.helen.placeaddresschecker.di.RepositoryModule
import com.helen.placeaddresschecker.di.ViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PlaceAddressChecker : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@PlaceAddressChecker)
            modules(
                listOf(
                    DataModule.getDependencies(),
                    RepositoryModule.getDependencies(),
                    ViewModelModule.getDependencies()
                )
            )
        }
    }
}