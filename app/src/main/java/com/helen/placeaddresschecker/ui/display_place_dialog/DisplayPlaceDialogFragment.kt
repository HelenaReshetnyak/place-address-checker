package com.helen.placeaddresschecker.ui.display_place_dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.helen.placeaddresschecker.R
import kotlinx.android.synthetic.main.dialog_fragment_display_place.view.*

class DisplayPlaceDialogFragment : DialogFragment(),  View.OnClickListener  {

    private var displayPlaceListener: DisplayPlaceListener? = null

    lateinit var placeName : String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val customView = inflater.inflate(R.layout.dialog_fragment_display_place, null)

        customView.content.text = placeName
        customView.setOnClickListener {
            displayPlaceListener?.onClose()
            dismiss()
        }
        return customView
    }

    override fun onClick(v: View?) {
        dismiss()
    }

    fun setListener(displayPlaceListener: DisplayPlaceListener) {
        this.displayPlaceListener = displayPlaceListener
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        displayPlaceListener?.onClose()
    }

    companion object {
       fun show (manager: FragmentManager, placeName: String) {
           DisplayPlaceDialogFragment().apply {
               this.placeName = placeName
               setListener(object : DisplayPlaceListener {
                   override fun onClose() {}
               })
               show(manager, DisplayPlaceDialogFragment::class.java.simpleName)
           }
       }
    }

    interface DisplayPlaceListener {
        fun onClose()
    }
}