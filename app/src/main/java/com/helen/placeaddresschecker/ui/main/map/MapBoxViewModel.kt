package com.helen.placeaddresschecker.ui.main.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.helen.placeaddresschecker.base.BaseViewModel
import com.helen.placeaddresschecker.model.GeocodingResponse
import com.helen.placeaddresschecker.repository.GeocodingRepository
import com.mapbox.mapboxsdk.geometry.LatLng
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class MapBoxViewModel : BaseViewModel(), KoinComponent {
    private val geocodingRepository: GeocodingRepository by inject()
    private val onGeocodingResponse : MutableLiveData<GeocodingResponse> = MutableLiveData()
    private val onError : MutableLiveData<Exception> = MutableLiveData()

    fun reversGeocode(location: LatLng) = launch {
        try {
            geocodingRepository.reverseGeocode(location).run {
                onGeocodingResponse.postValue(this)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            onError.postValue(ex)
        }
    }

    fun observeResponse() : LiveData<GeocodingResponse> = onGeocodingResponse

    fun observeError() : LiveData<Exception> = onError
}