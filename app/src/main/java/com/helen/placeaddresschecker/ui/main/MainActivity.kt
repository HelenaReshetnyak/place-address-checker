package com.helen.placeaddresschecker.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.helen.placeaddresschecker.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}