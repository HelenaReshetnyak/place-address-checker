package com.helen.placeaddresschecker.ui.error_dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.helen.placeaddresschecker.R
import kotlinx.android.synthetic.main.dialog_fragment_error.view.*

class ErrorDialogFragment : DialogFragment(),  View.OnClickListener  {

    private var errorListener: ErrorListener? = null

    lateinit var errorMessage: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val customView = inflater.inflate(R.layout.dialog_fragment_error, null)

        customView.errorMessage.text = String.format("%s %s", getString(R.string.error), errorMessage)
        customView.setOnClickListener {
            errorListener?.onClose()
            dismiss()
        }
        return customView
    }

    override fun onClick(v: View?) {
        dismiss()
    }

    fun setListener(errorListener: ErrorListener) {
        this.errorListener = errorListener
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        errorListener?.onClose()
    }

    companion object {
        fun show (manager: FragmentManager, errorMessage: String) {
            ErrorDialogFragment().apply {
                this.errorMessage = errorMessage
                setListener(object : ErrorListener {
                    override fun onClose() {}
                })
                show(manager, ErrorDialogFragment::class.java.simpleName)
            }
        }
    }

    interface ErrorListener {
        fun onClose()
    }
}
