package com.helen.placeaddresschecker.ui.main.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.res.Resources
import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.helen.placeaddresschecker.R
import com.helen.placeaddresschecker.exception.AddressNotFoundException
import com.helen.placeaddresschecker.ui.display_place_dialog.DisplayPlaceDialogFragment
import com.helen.placeaddresschecker.ui.error_dialog.ErrorDialogFragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.mapbox.api.geocoding.v5.GeocodingCriteria
import com.mapbox.api.geocoding.v5.MapboxGeocoding
import com.mapbox.api.geocoding.v5.models.GeocodingResponse
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.fragment_map_box.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MapBoxFragment : Fragment() {

    private var mapBoxMap: MapboxMap? = null
    private val viewModel: MapBoxViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Mapbox.getInstance(requireContext(), getString(R.string.maps_token))
        return inflater.inflate(R.layout.fragment_map_box, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        initMaps()
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(
            requireContext(),
            getString(R.string.tap_to_find),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun initMaps(){
        mapView.getMapAsync { mapBoxMap ->
            this.mapBoxMap = mapBoxMap
            mapBoxMap.setStyle(Style.MAPBOX_STREETS) { style ->
                runWithPermissions(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) {
                    initLocationComponent(style)
                    initMapClickListener()
                }

            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun initLocationComponent(style: Style){
        mapBoxMap?.run {

            val locationComponent = this.locationComponent

            locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(
                    requireContext(),
                    style
                ).build()
            )
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.TRACKING
            locationComponent.renderMode = RenderMode.COMPASS
        }
    }

    private fun initMapClickListener(){
        mapBoxMap?.addOnMapClickListener { location ->
            viewModel.reversGeocode(location)
            return@addOnMapClickListener true
        }
    }

    private fun observeViewModel() {
        viewModel.observeResponse().observe(viewLifecycleOwner, { response ->
            DisplayPlaceDialogFragment.show(requireActivity().supportFragmentManager, response.placeName)
        })

        viewModel.observeError().observe(viewLifecycleOwner, {
            when(it) {
                AddressNotFoundException -> ErrorDialogFragment.show(requireActivity().supportFragmentManager, getString(R.string.error_not_found))
                else -> ErrorDialogFragment.show(requireActivity().supportFragmentManager, getString(R.string.error_something_went_wrong))
            }
        })
    }
}



