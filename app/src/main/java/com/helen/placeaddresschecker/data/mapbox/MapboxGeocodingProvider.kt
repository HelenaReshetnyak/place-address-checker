package com.helen.placeaddresschecker.data.mapbox

import com.helen.placeaddresschecker.data.provider.GeocodingProvider
import com.helen.placeaddresschecker.exception.AddressNotFoundException
import com.mapbox.api.geocoding.v5.GeocodingCriteria
import com.mapbox.api.geocoding.v5.MapboxGeocoding
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.api.geocoding.v5.models.GeocodingResponse
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class MapboxGeocodingProvider : GeocodingProvider<LatLng, CarmenFeature> {

    override suspend fun reverseGeocoding(location: LatLng): CarmenFeature = suspendCoroutine { continuation ->
        getGeocodingClient(location.longitude, location.latitude)
            .enqueueCall(object : Callback<GeocodingResponse> {
                override fun onResponse(
                    call: Call<GeocodingResponse>,
                    response: Response<GeocodingResponse>) {
                    response.body()
                        ?.features()
                        ?.takeIf { it.isNotEmpty() }
                        ?.first()
                        ?.run {
                            continuation.resume(this)
                        } ?: continuation.resumeWithException(AddressNotFoundException)

                }

                override fun onFailure(call: Call<GeocodingResponse>, t: Throwable) {
                    continuation.resumeWithException(t)
                }
            })
    }

    private fun getGeocodingClient(
        longitude: Double,
        latitude: Double,
        accessToken: String = MAPBOX_TOKEN
    ) =
        MapboxGeocoding.builder()
            .accessToken(accessToken)
            .query(Point.fromLngLat(longitude, latitude))
            .geocodingTypes(GeocodingCriteria.TYPE_POI)
            .build()

    companion object {
        private const val MAPBOX_TOKEN = "pk.eyJ1IjoiaGVsZW5hcmVzaGV0bnlhayIsImEiOiJjazFta25pcHgwMWt0M2NrMjd4dzY4MGptIn0.e5MxtwZKDmkkhoUDf4cnmA"
    }
}