package com.helen.placeaddresschecker.data.provider

interface GeocodingProvider<Location, GeocodingResponse> {
    suspend fun reverseGeocoding(location: Location) : GeocodingResponse
}