package com.helen.placeaddresschecker.storage

import com.helen.placeaddresschecker.data.provider.GeocodingProvider
import com.helen.placeaddresschecker.model.GeocodingResponse
import com.helen.placeaddresschecker.repository.GeocodingRepository
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.mapboxsdk.geometry.LatLng
import org.koin.core.KoinComponent
import org.koin.core.inject

class GeocodingStorage : GeocodingRepository, KoinComponent {

    private val geocodingProvider : GeocodingProvider<LatLng, CarmenFeature> by inject()

    override suspend fun reverseGeocode(location: LatLng) : GeocodingResponse {
        return geocodingProvider.reverseGeocoding(location).run {
            GeocodingResponse(
                placeName() ?: DEFAULT_PLACE_NAME,
                address() ?: DEFAULT_ADDRESS
            )
        }
    }

    companion object {
        private const val DEFAULT_PLACE_NAME = "Not found any places"
        private const val DEFAULT_ADDRESS = "Not found any addresses"
    }
}